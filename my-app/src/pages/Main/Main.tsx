import React, { useEffect, memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import ProductCard from '../../components/ProductCard';
import Filters from '../../components/Filters';

import { getProducts } from '../../store/actions/actions';

import styles from './Main.module.scss';

interface State {
  products: {
    products: [
      {
        id: number,
        link: string,
        code: number,
        imgUrl: string,
        availability: boolean,
        title: string,
        params: [
          {
            name: string,
            value: string
          }
        ],
        inFav: boolean,
        inComparsion: boolean
      }
    ]
  }
}

function Main() {
  const dispatch = useDispatch();
  const { products } = useSelector((state: State) => state.products);

  useEffect(() => {
    if (!products.length) {
      dispatch(getProducts());
    }
  });

  const productList = products.map((product) => (
    <li className={styles.listItem} key={product.id}>
      <ProductCard product={product} />
    </li>
  ));

  return (
    <div className={styles.main}>
      <div className={styles.productList}>
        {products && productList}
      </div>
      <Filters />
    </div>
  );
}

export default memo(Main);
