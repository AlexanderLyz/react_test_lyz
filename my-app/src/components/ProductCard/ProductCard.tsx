import React, { memo } from 'react';

import ProductAvailable from '../ProductAvailable';
import ComparisonButton from '../ComparisonButton';
import BuyButton from '../BuyButton';
import FavButton from '../FavButton';

import styles from './ProductCard.module.scss';

interface Props {
  product: {
    id: number,
    link: string,
    code: number,
    imgUrl: string,
    availability: boolean,
    title: string,
    params: Array<{name: string, value: string}>,
    inFav: boolean,
    inComparsion: boolean
  }
}

function ProductCard(props: Props) {
  const { product } = props;
  const paramsList = product.params && product.params.map((elem) => (
    <li className={styles.params} key={elem.name}>
      <span className={styles.paramsName}>{elem.name}</span>
      <span className={styles.paramsValue}>{elem.value}</span>
    </li>
  ));
  return (
    <div className={styles.product}>
      <p className={styles.art}>
        Арт. {product.code}
      </p>
      <img className={styles.image} src={`images/${product.imgUrl}`} alt="product" />
      <div className={styles.content}>
        {product.availability && <ProductAvailable />}
        <p className={styles.title}>
          {product.title}
        </p>
        <ul className={styles.list}>
          {paramsList}
        </ul>
        <div className={styles.buttons}>
          <BuyButton />
          <div className={styles.rightButtons}>
            <FavButton id={product.id} />
            <ComparisonButton inComparison={product.inComparsion} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default memo(ProductCard);
