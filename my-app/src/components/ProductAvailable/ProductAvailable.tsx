import React, { memo } from 'react';

import styles from './ProductAvailable.module.scss';

function ProductAvailable() {
  return (
    <div className={styles.available}>
      <img src="images/check.svg" alt="check" />
      <span className={styles.availableText}>В наличии</span>
    </div>
  );
}

export default memo(ProductAvailable);
