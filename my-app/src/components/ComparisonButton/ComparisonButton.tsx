import React, { memo } from 'react';

import styles from './ComparisonButton.module.scss';

interface Props {
  inComparison: boolean;
}

function ComparisonButton(props: Props) {
  return (
    <div className={styles.button}>
      {props.inComparison ? <img src="images/comparsion.svg" alt="" /> : <img src="images/scales-26.jpg" alt="comp" />}
    </div>
  );
}

export default memo(ComparisonButton);
