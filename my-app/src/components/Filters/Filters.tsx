import React, { SyntheticEvent, useState, memo } from 'react';
import { useDispatch } from 'react-redux';

import { submitFilters, getProducts } from '../../store/actions/actions';

import styles from './Filters.module.scss';

function Filters() {
  const dispatch = useDispatch();
  const [filters, setFilters] = useState<string[]>([]);

  const handleSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    filters.length
      ? dispatch(submitFilters(filters))
      : dispatch(getProducts());
  };

  const handleReset = () => {
    setFilters([]);
  };

  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    const { id } = event.target as HTMLInputElement;
    if (filters.includes(id)) {
      setFilters(filters.filter((item) => item !== id));
    } else {
      setFilters([...filters, id]);
    }
  };

  return (
    <form className={styles.filters} onSubmit={handleSubmit}>
      <input className={styles.submitButton} type="submit" value="Показать результат" />
      <input className={styles.resetButton} type="reset" value="Очистить фильтр" onClick={handleReset} />
      <div className={styles.content}>
        <span className={styles.title}>Производитель</span>
        <div className={styles.row}>
          <div className={styles.element}>
            <input type="checkbox" id="Canon" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Canon">Canon</label>
          </div>
          <div className={styles.element}>
            <input type="checkbox" id="Olympus" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Olympus">Olympus</label>
          </div>
        </div>
        <div className={styles.row}>
          <div className={styles.element}>
            <input type="checkbox" id="Fujifilm" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Fujifilm">Fujifilm</label>
          </div>
          <div className={styles.element}>
            <input type="checkbox" id="Pentax" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Pentax">Pentax</label>
          </div>
        </div>
        <div className={styles.row}>
          <div className={styles.element}>
            <input type="checkbox" id="Nikon" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Nikon">Nikon</label>
          </div>
          <div className={styles.element}>
            <input type="checkbox" id="Sigma" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Sigma">Sigma</label>
          </div>
        </div>
        <div className={styles.row}>
          <div className={styles.element}>
            <input type="checkbox" id="Panasonic" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Panasonic">Panasonic</label>
          </div>
          <div className={styles.element}>
            <input type="checkbox" id="GeneralElectrics" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="GeneralElectrics">General Electrics</label>
          </div>
        </div>
        <div className={styles.row}>
          <div className={styles.element}>
            <input type="checkbox" id="Leica" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Leica">Leica</label>
          </div>
          <div className={styles.element}>
            <input type="checkbox" id="Zenit" className={styles.checkbox} onChange={handleChange} />
            <label htmlFor="Zenit">Zenit</label>
          </div>
        </div>
      </div>
    </form>
  );
}

export default memo(Filters);
