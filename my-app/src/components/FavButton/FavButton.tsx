import React, { memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { addToFav } from '../../store/actions/actions';
import { State } from '../../store/reducers';

import styles from './FavButton.module.scss';

interface Props {
  id: number
}

function FavButton(props: Props) {
  const dispatch = useDispatch();
  const { products } = useSelector((state: State) => state.products);
  const { id } = props;
  const product = products.find((item) => item.id === id);
  const inFav = product && product.inFav;

  function handleClick() {
    dispatch(addToFav(id));
  }

  return (
    <button type="button" className={styles.button} onClick={handleClick}>
      {inFav ? <img className={styles.image} src="images/favourite-active.svg" alt="fav" /> : <img src="images/shape.svg" alt="fav" />}
    </button>
  );
}

export default memo(FavButton);
