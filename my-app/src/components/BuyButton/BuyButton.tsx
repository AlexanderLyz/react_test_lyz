import React, { memo } from 'react';

import styles from './BuyButton.module.scss';

function BuyButton() {
  return (
    <>
      <button className={styles.buyButton} type="button">
        <img src="images/cart.png" alt="buy" />
        <span className={styles.text}>Купить</span>
      </button>
    </>
  );
}

export default memo(BuyButton);
