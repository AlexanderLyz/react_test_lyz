import { createAction } from 'typesafe-actions';

import { GET_PRODUCTS_IN_START, ADD_PRODUCT_TO_FAV, SUBMIT_FILTERS } from './actionTypes';

export interface Action {
  type: string;
  payload?: unknown;
}

export const getProducts = createAction(GET_PRODUCTS_IN_START)<void>();
export const addToFav = createAction(ADD_PRODUCT_TO_FAV)<number>();
export const submitFilters = createAction(SUBMIT_FILTERS)<string[]>();
