import { call, put } from 'redux-saga/effects';

import { GET_PRODUCTS_IN_SUCCESS } from '../actions/actionTypes';
import instance from './instance';

export default function* getProducts() {
  const response = yield call(instance, '/PRODUCTS_SUCCESS', {
    method: 'get',
  });
  if (!response.data.success) {
    console.log(response.data.status);
  } else {
    const payload = response.data.data.products;
    yield put({ type: GET_PRODUCTS_IN_SUCCESS, payload });
  }
}
