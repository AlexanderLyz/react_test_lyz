import { call, put } from 'redux-saga/effects';

import { GET_PRODUCTS_IN_SUCCESS } from '../actions/actionTypes';
import { Action } from '../actions/actions';
import instance from './instance';

export default function* submitFilters(action: Action) {
  const filters = action.payload;
  const response = yield call(instance, '/FILTER_SUCCESS', {
    method: 'get',
    data: { filters },
  });
  if (!response.data.success) {
    console.log(response.data.status);
  } else {
    const payload = response.data.data.products;
    yield put({ type: GET_PRODUCTS_IN_SUCCESS, payload });
  }
}
