import { takeEvery, all, fork } from 'redux-saga/effects';

import {
  GET_PRODUCTS_IN_START, ADD_PRODUCT_TO_FAV, SUBMIT_FILTERS,
} from '../actions/actionTypes';

import getProducts from './getProducts';
import addToFav from './addToFav';
import submitFilters from './submitFilters';

function* watcherProducts() {
  yield takeEvery(GET_PRODUCTS_IN_START, getProducts);
  yield takeEvery(ADD_PRODUCT_TO_FAV, addToFav);
  yield takeEvery(SUBMIT_FILTERS, submitFilters);
}

function* rootSaga() {
  yield all([fork(watcherProducts)]);
}

export default rootSaga;
