import { call, put } from 'redux-saga/effects';

import { ADD_PRODUCT_TO_FAV_SUCCESS } from '../actions/actionTypes';
import { Action } from '../actions/actions';
import instance from './instance';

export default function* addToFav(action: Action) {
  const id = action.payload;
  const response = yield call(instance, '/FAVORITE_SUCCESS', {
    method: 'get',
    data: { productID: id },
  });
  if (!response.data.data.inFav) {
    console.log(response.data.data.message);
  } else {
    yield put({ type: ADD_PRODUCT_TO_FAV_SUCCESS, payload: id });
  }
}
