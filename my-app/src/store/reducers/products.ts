import { createReducer } from 'typesafe-actions';

import { GET_PRODUCTS_IN_SUCCESS, ADD_PRODUCT_TO_FAV_SUCCESS } from '../actions/actionTypes';

import { Action } from '../actions/actions';

interface Product {
  id: number,
  link: string,
  code: number,
  imgUrl: string,
  availability: boolean,
  title: string,
  params: Array<{name: string, value: string}>,
  inFav: boolean,
  inComparsion: boolean
}

export interface State {
  products: Array<Product>;
  error: null | string;
}

const defaultState = {
  products: [],
  error: null,
};

const productsReducer = createReducer(defaultState)
  .handleType(GET_PRODUCTS_IN_SUCCESS,
    (state: State, action: Action) => ({
      products: action.payload, error: null,
    }))
  .handleType(ADD_PRODUCT_TO_FAV_SUCCESS,
    (state: State, action: Action) => ({
      ...state,
      products: (state.products.map((product: Product) => {
        if (product.id === action.payload) {
          const favProduct = { ...product };
          favProduct.inFav = true;
          return favProduct;
        }
        return product;
      })),
      error: null,
    }));

export default productsReducer;
