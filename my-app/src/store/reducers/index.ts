import { combineReducers } from 'redux';

import productsReducer, { State as ProductState } from './products';

export interface State {
  products: ProductState
}

const rootReducer = combineReducers({
  products: productsReducer,
});

export default rootReducer;
